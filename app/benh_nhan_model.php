<?php

namespace App;
use DB;
use Illuminate\Database\Eloquent\Model;

class benh_nhan_model extends Model
{
    public $ma_benh_nhan;
    public $ten_benh_nhan;
    public $email;
    public $mat_khau;
    public $sdt;
    public $dia_chi;
    public $gioi_tinh;
    public $ngay_sinh;
    
    static function get_all()
    {
    	$sql="select * from benh_nhan";
    	$result=DB::select($sql);
    	// dd($result);
    	return $result;
    }
     public function process_insert(){
     $sql = "insert into benh_nhan values('$this->ma_benh_nhan','$this->ten_benh_nhan','$this->email','$this->mat_khau')";
     DB::insert($sql);
    }
    public function get_one(){
      $result = DB::select('select * from benh_nhan where email = ? and mat_khau = ?',[
        $this->email,
        $this->mat_khau
      ]);
      return $result;
  }
    public function sua_xu_ly(){
    $sql ="update benh_nhan set ten_benh_nhan ='$this->ten_benh_nhan',email='$this->email',mat_khau='$this->mat_khau' where ma_benh_nhan='$this->ma_benh_nhan'";
     DB::update($sql);
  }
}
