<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\demo_model;
use Session;


class Admin_controller extends Controller
{
 	public function view_login(){
 	      return view('login'); 
 	}

    public function process_login(Request $rq)
    {
     	$admin = new demo_model();
     	$admin->email     =$rq->email;
     	$admin->mat_khau  =$rq->mat_khau;
     	$arr_account = $admin->get_one();
    	
     	if (count($arr_account) == 1) {
     		$rq->session()->put('ma_admin',$arr_account[0]->ma_admin);
     		$rq->session()->put('ten_admin',$arr_account[0]->ten_admin);
     		$id = Session::get('ma_admin');
     		return redirect()->route('admin.infor');
     	}
     	else {
     		return redirect()->route('view_login');
     	} 
    }

    public function infor()
    {
    	$result = demo_model::get_all();
    	return view('admin.infor',compact('result')) ;

    }

    public function layout(){
		$id = Session::get('ma_admin');
		return view('admin.layout', compact('id')); 
	}


	public function admin_editor($ma_admin){
	 	$admin = new demo_model();
	 	$id = Session::get('ma_admin');
	 	$admin->ma_admin = $id;
	 	$info = demo_model::get_info($id);
	 	return view('admin.admin_editor',compact('id'), compact('info'));
	}

	public function handle(request $rq){
	  	$admin             = new demo_model();
	 	$admin->ma_admin   =$rq->ma_admin;
	 	$admin->ten_admin  =$rq->ten_admin;
	 	$admin->email      =$rq->email;
	 	$admin->mat_khau   =$rq->mat_khau;
	 	$admin->handle();
	 	return redirect()->route('admin.infor');
	}
 

     public function logout(Request $rq){
 	   	$rq->session()->flust();
 	   	return redirect()->route('admin.login');
 	}

	// public function xoa_admin($ma_admin){
	//  	$admin = new demo_model();
	//  	$admin->ma_admin = $ma_admin;
	//  	$arr_admin = $admin->xoa_admin();
	//  	return redirect()->route('danhsach_admin');
	// }	

 // 	public  function them_xu_ly(request $rq ){
	//  	$admin            = new demo_model();
	//  	$admin->ma_admin  =$rq->ma_admin;
	//  	$admin->ten_admin =$rq->ten_admin;
	//  	$admin->email     =$rq->email;
	//  	$admin->mat_khau  =$rq->mat_khau;
	//  	$admin->process_insert();
	//  	return redirect()->route('admin.danhsach_admin');
	// }
 }
