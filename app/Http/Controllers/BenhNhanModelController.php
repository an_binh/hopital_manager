<?php

namespace App\Http\Controllers;

use App\benh_nhan_model;
use Illuminate\Http\Request;

class BenhNhanModelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\benh_nhan_model  $benh_nhan_model
     * @return \Illuminate\Http\Response
     */
    public function show(benh_nhan_model $benh_nhan_model)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\benh_nhan_model  $benh_nhan_model
     * @return \Illuminate\Http\Response
     */
    public function edit(benh_nhan_model $benh_nhan_model)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\benh_nhan_model  $benh_nhan_model
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, benh_nhan_model $benh_nhan_model)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\benh_nhan_model  $benh_nhan_model
     * @return \Illuminate\Http\Response
     */
    public function destroy(benh_nhan_model $benh_nhan_model)
    {
        //
    }
}
