<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\benh_nhan_model;
class Patient_controller extends Controller
{
   public function view()
    {
        $result = benh_nhan_model::get_all();
        return view('patient.list_viewer',compact('result')) ;

    }

    public function them_benh_nhan(){
        return view('them_benh_nhan'); 
    }
    // public function layout(){
    //     return view('benh_nhan.layout'); 
    // }
    
    public  function them_xu_ly(request $rq ){
        $benh_nhan            = new benh_nhan_model();
        $benh_nhan->ma_benh_nhan  =$rq->ma_benh_nhan;
        $benh_nhan->ten_benh_nhan =$rq->ten_benh_nhan;
        $benh_nhan->email     =$rq->email;
        $benh_nhan->mat_khau  =$rq->mat_khau;
        
        $abenh_nhan->process_insert();
        return redirect()->route('benh_nhan.danhsach_benh_nhan');
    }

    public function sua_benh_nhan($ma_benh_nhan){
         $benh_nhan = new demo_model();
         $benh_nhan->ma_admin = $ma_benh_nhan;
         $arr_benh_nhan = $benh_nhan->get_one();
         return view('sua_benh_nhan',compact('arr_benh_nhan'));
    }

    public function sua_xu_ly(request $rq){
         $benh_nhan             = new demo_model();
         $benh_nhan->ma_admin   =$rq->ma_benh_nhan;
         $benh_nhan->ten_admin  =$rq->ten_benh_nhan;
         $benh_nhan->email      =$rq->email;
         $benh_nhan->mat_khau   =$rq->mat_khau;
         $benh_nhan->sdt  =$rq->sdt;
         
         $benh_nhan->sua_xu_ly();
         return redirect()->route('danhsach_benh_nhan');
    }
} 
     

     

