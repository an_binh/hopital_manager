<?php

use Illuminate\Support\Facades\Route;


Route::get('/', function () {
     return view('home');
});    

Route::get('login','Admin_controller@view_login')->name('view_login');	
Route::post('process_login','Admin_controller@process_login')->name('process_login');
Route::group(['prefix'=>'admin', 'as'=>'admin.','middleware' => [ 'CheckLogin' ] ], function () {

	Route::get('infor','Admin_controller@infor')->name('infor');
    Route::get('admin_editor/{ma_admin}','Admin_controller@admin_editor')->name('admin_editor');
	Route::post('admin_editor/{ma_admin}','Admin_controller@handle')->name('handle');
	
//benhnhan
	Route::get('patients','Patient_controller@view')->name('patients');



// doctor

   Route::get('doctors','doctor_controller@doctors')->name('doctors');

   Route::get('add_doctor','doctor_controller@add_doctor')->name('add_doctor');

   Route::post('process_add_doctor','doctor_controller@process_add_doctor')->name('process_add_doctor');

   Route::get('view_update_doctor/{ma_bac_si}','doctor_controller@view_update_doctor')->name('view_update_doctor');

   Route::post('process_update_doctor/{ma_bac_si}','doctor_controller@process_update_doctor')->name('process_update_doctor');

   Route::get('delete_doctor/{ma_bac_si}','doctor_controller@delete_doctor')->name('delete_doctor');


   

 // Route::get('xoa_admin/{ma_admin}','Admin_controller@xoa_admin')->name('admin.xoa_admin');

	 // Route::get('them_admin','Admin_controller@them_admin')->name('them_admin');
  //    Route::post('them_admin','Admin_controller@them_xu_ly')->name('admin.them_xu_ly');

	// Route::get('them_benh_nhan','benh_nhan_controller@them_benh_nhan')->name('them_benh_nhan');
	// Route::post('them_benh_nhan','benh_nhan_controller@them_xu_ly')->name('benh_nhan.them_xu_ly');

	// Route::get('sua_benh_nhan/{ma_benh_nhan}','benh_nhan_controller@sua_benh_nhan')->name('benh_nhan.sua_benh_nhan');
	// Route::post('sua_benh_nhan/{ma_benh_nhan}','benh_nhan_controller@sua_xu_ly')->name('benh_nhan.sua_xu_ly');

	 

});












 