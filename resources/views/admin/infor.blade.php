@extends('admin.layout')
@section('view')

<style>
.card {
  position: absolute;
  right: -500px;
  width: 300px;
  border: 3px solid #73AD21;
  padding: 10px;
}
</style>


<div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-6 col-md-12">
                            <div class="card">
                                <div class="card-content table-responsive">
                                    <table class="table table-hover">
                                        <p> My home </p>
                                        <thead class="text-warning">
                                            <th>ID</th>
                                            <th>Name</th>
                                            <th>Email</th>
                                        </thead>
                                        <tbody>
                                            <h2>Thông tin tài khoản</h2>
                                            <?php foreach ($result as $each): ?>
                                                <tr>
                                                    <td> {{$each->ma_admin }} </td>
                                                    <td> {{$each->ten_admin }} </td>
                                                    <td> {{$each->email }} </td>
                                                </tr>
                                                
                                            <?php endforeach ?> 
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
</div>